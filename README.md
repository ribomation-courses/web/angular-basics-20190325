Angular och TypeScript, 3 dagar
====

Välkommen till denna kurs. Här finner du:
* Installationsanvisningar
* Lösningar till programmeringsuppgifterna (_laddas upp under kursens gång_)


Installationsanvisningar
====

För att utföra kursen programmeringsuppgifter behöver du följande programvaror:

* NodeJS / NPM
* En modern webbläsare
* En modern IDE för Angular utveckling
* GIT klient

NodeJS / NPM
----
Installera NodeJS från 
* https://nodejs.org/en/download/

Välj en LTS version, såsom version 8 eller 10.
Efter installationen, verifiera i ett kommandoradsfönster:

    node --version
    npm --version

Installera sedan Angular CLI

    npm install --global @angular/cli

Samt, verifiera

    ng --version


Modern webbläsare
----
Se till att du har någon av följande installerad:
- [Google Chrome](https://www.google.com/chrome/browser/desktop/index.html)
- [Mozilla FireFox](https://www.mozilla.org/sv-SE/firefox/new/)
- [MicroSoft Edge](https://www.microsoft.com/sv-se/windows/microsoft-edge)

Notera att, MS Internet Explorer _inte_ fungerar med Angular.


Modern IDE
----
Installera en av följande

* [JetBrains WebStorm (_30 dgr utvärdering_)](https://www.jetbrains.com/webstorm/download)
* [MicroSoft Visual Code](https://code.visualstudio.com/)


GIT klient
----
Installera en klient för GIT och använd gärna den BASH terminal som
följer med _Git for Windows_
- [GIT Client](https://git-scm.com/downloads)

Skapa en baskatalog för denna kurs, med två underkataloger.
Den ena innehåller dina egan lösningar, en katalog per kapitel. Den
andra innehåller denna GIT repo.

    mkdir -p ~/angular-course/my-solutions
    cd ~/angular-course
    git clone https://gitlab.com/ribomation-courses/web/angular-basics-20190325.git gitlab

Efter att nya lösningar laddats upp (`git push`), så kan du ladda
ned uppdateringar med:

    cd ~/angular-course/gitlab
    git pull


Installera NPM paket
====
Innan du kan start upp någon av lösningar, behöver du ladda ned all
NPM paket. Det gör du med kommandot:

    npm install

***
Om du har några fundering, kontakta mig:

**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

