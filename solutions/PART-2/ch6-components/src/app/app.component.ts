import {Component} from '@angular/core';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent {
  name: string  = 'Justin Time';
  email: string = 'justin@gmail.com';
  url: string   = undefined;

  onSave(propertyName: string, payload: any): void {
    console.info('[onSave]', propertyName, payload);
  }

}
