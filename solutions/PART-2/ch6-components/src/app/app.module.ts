import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent }          from './app.component';
import { EditableTextComponent } from './editable-text/editable-text.component';
import {FormsModule}             from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    EditableTextComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
