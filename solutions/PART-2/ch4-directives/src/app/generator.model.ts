import {Vehicle} from './vehicle.model';

const names = [
  'Volvo', 'SAAB', 'Jeep', 'BMW', 'Mercedes', 'Lexus',
  'Audi', 'Jaguar', 'Range Rover'
];

const icons = [
  'bicycle', 'bus', 'car', 'motorcycle', 'plane', 'ship',
  'subway', 'taxi', 'truck'
];

function randInt(lb: number, ub: number): number {
  return Math.floor(lb + (ub - lb + 1) * Math.random());
}

export class Generator {
  static mk(): Vehicle {
    let v = new Vehicle();
    v.price = randInt(1000, 10000);
    v.name = names[randInt(0, names.length - 1)];
    v.icon = icons[randInt(0, icons.length - 1)];
    return v;
  }

  static generate(n: number): Vehicle[] {
    const lst = [];
    for (let k = 0; k < n; ++k) {
      lst.push(this.mk());
    }
    return lst;
  }
}
