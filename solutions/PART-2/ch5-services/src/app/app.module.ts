import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent }             from './app.component';
import {SHIPPING_COST, VAT_PERCENT} from './tokens';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    {provide: VAT_PERCENT, useValue: 25},
    {provide: SHIPPING_COST, useValue: 95},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
