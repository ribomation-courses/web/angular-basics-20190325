import {Component} from '@angular/core';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent {
  url: string = environment.image;
  devMode: boolean = environment.production === false;
}
