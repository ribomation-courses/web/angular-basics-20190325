import {Component} from '@angular/core';
import {Book, Generator} from '../Product';
import {Router} from '@angular/router';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent {
  books: Book[] = [];

  constructor(private router: Router) {
    this.books = Generator.books();
  }

  view(id: number): void {
    this.router.navigate(['/products/detail', {id: id}]);
  }

}
