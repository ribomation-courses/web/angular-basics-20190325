import {Component, Input, OnInit} from '@angular/core';
import {Account}                  from '../account';

@Component({
  selector:    'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls:   ['./account-list.component.scss']
})
export class AccountListComponent implements OnInit {
  @Input('list') accounts: Account[] = [];

  constructor() {
  }

  ngOnInit() {
  }

  get total(): number {
    return this.accounts.reduce((sum: number, a: Account) => sum + a.balance, 0);
  }

}
