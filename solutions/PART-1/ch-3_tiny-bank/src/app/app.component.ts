import {Component} from '@angular/core';
import {Account}   from './account';
import {Generator} from './generator';
import {generate}  from 'rxjs';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent {
  accounts: Account[] = [];
  count: number       = 5;

  constructor() {
    this.generate(this.count);
  }

  generate(n: number) {
    console.log('generate', n);
    this.accounts = [];
    while (n-- > 0) {
      this.accounts.push(Generator.create());
    }
  }

}
