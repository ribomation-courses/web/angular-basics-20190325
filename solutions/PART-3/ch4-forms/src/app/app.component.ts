import {Component}                          from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector:    'app-root',
  templateUrl: './app.component.html',
  styleUrls:   ['./app.component.css']
})
export class AppComponent {
  signupForm: FormGroup;
  notificationMessage: string;

  constructor(formBuilder: FormBuilder) {
    this.setup(formBuilder);
  }

  setup(formBuilder: FormBuilder): void {
    this.signupForm = formBuilder.group({
        name:    ['', [
          Validators.required,
          Validators.minLength(3)]],
        email:   ['', [
          Validators.required,
          Validators.email
        ]],
        address: ['', [
          Validators.required,
          Validators.minLength(5)
        ]],
      }
    );
  }

  submit(): void {
    if (this.signupForm.valid) {
      console.info('form-data', this.signupForm.value);
      this.signupForm.reset();
      this.notificationMessage = 'Thank you very much !!!';

      setTimeout(() => {
        this.notificationMessage = undefined;
      }, 4000);
    } else {
      console.warn('form invalid',
        this.signupForm.status,
        this.signupForm.value);
    }
  }
}
