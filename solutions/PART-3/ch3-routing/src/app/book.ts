export class Book {
  id: number;
  name: string;
  pages: number;
  price: number;
  itemsInStore: number;
  lastUpdated: Date;
}
